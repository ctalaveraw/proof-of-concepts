# Experiments branch

These are where I stored my first ever Git repository branches:

- crazynflostats - first repository, just adding NFL statistics
- crazynfldstats - cloned from first repo and added as another branch

I kept the commands that I ran that day as a reminder of how far I've come.

Any additional experiments will have their own seperate branch created.
